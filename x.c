// Example usage of the test harness.

#include "test.h"

int main(int argc, char* argv[]) {
  if (argc == 2)
    return run_single_test(argv[1]);
  return run_tests();
}

// no initialization needed before each test
void reset(void) {}

// Example tests.

void test_1(void) {
  CHECK(true);
}

void test_2(void) {
  CHECK(2+2 == 4);
}

// failing test
void test_3(void) {
  CHECK(2+2 == 5);
}

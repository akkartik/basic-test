#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "test.h"

#include "function_list"  // list of function prototypes auto-generated in 'build'
const test_fn Tests[] = {
  #include "test_list"  // list of test function names auto-generated in 'build'
};

bool Passed = true;
int Num_failures = 0;

int run_tests(void) {
  for (size_t i = 0;  i < sizeof(Tests)/sizeof(Tests[0]);  ++i)
    run_test(i);
  fprintf(stderr, "\n");
  if (Num_failures > 0) {
    fprintf(stderr, "%d failure%s\n", Num_failures, (Num_failures > 1 ? "s" : ""));
    return 1;
  }
  return 0;
}

void run_test(int i) {
  Passed = true;
  reset();
  (*Tests[i])();
  if (Passed) fprintf(stderr, ".");
  else ++Num_failures;
}

const char* Test_names[] = {
  #include "test_name_list"  // auto-generated; see 'build*' scripts
};

int run_single_test(const char* test_name) {
  for (size_t i=0;  i < sizeof(Tests)/sizeof(Tests[0]);  ++i) {
    if (strcmp(Test_names[i], test_name) != 0) continue;
    run_test(i);
    if (Passed) fprintf(stderr, "\n");
    return !Passed;
  }
  return 1;  // no test found
}

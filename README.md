Example project showing a simple test harness for small projects in C.

#### Usage

0. Call `run_tests()` through main somehow, after doing your own argument
   parsing.

1. Call `run_single_test()` through main somehow, based on your argument
   parsing.

2. Put 'setup' code you want to run before each test into a function called
   `reset()`.

3. Write each test in a function whose first line has the rigid form:

```
void test_...(void) {
```

The entire prototype must fit on a single line, with the opening curly brace
'cuddled' on the same line. (This form allows us to easily autogenerate
prototypes for functions, so that you can define your functions and tests in
any order in any .c file.)

Read test.h for more details.

#### Try it out

First build the project, using either (preferred):

```
./build
```

or (more conventional):

```
make
```

Then try running tests.

```
$ ./a.out  # run some example tests
..  # emits one dot per passing test
$ ./a.out test_2  # run a single test
.  # single dot for single test
```

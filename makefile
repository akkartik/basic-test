CFLAGS ?= -g -O3

a.out: *.c *.h function_list test_list test_name_list
	${CC} ${CFLAGS} *.c

function_list: *.c
	grep -h "^[^ #].*) {" *.c  |sed 's/ {.*/;/' > function_list

test_list: *.c
	grep -h "^\s*void test_" *.c  |sed 's/^\s*void \(.*\)(void) {.*/\1,/' > test_list

test_name_list: *.c
	grep -h "^\s*void test_" *.c  |sed 's/^\s*void \(.*\)(void) {.*/"\1",/' > test_name_list

.PHONY: clean

clean:
	-rm -rf a.out* function_list test_list test_name_list
